package com.hackaton.loyaltycards.controllers;

import com.hackaton.loyaltycards.models.CardModel;
import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.services.CardService;
import com.hackaton.loyaltycards.services.CardServiceResponse;
import com.hackaton.loyaltycards.services.CompanyService;
import com.hackaton.loyaltycards.services.CompanyServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/loyalty/v1/card")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class CardController {

    @Autowired
    CardService cardService;

    @GetMapping("")
    public ResponseEntity<List<CardModel>> getAllCards(){
        System.out.println("getCards");
        return new ResponseEntity<>(this.cardService.findAll(), HttpStatus.OK);
    }
    @GetMapping("{pan}")
    public ResponseEntity<CardServiceResponse>getCardById(@PathVariable String pan){
        System.out.println("getCardById");
        System.out.println("El PAN es: "+pan);
        CardServiceResponse result = this.cardService.findById(pan);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    @DeleteMapping("{pan}")
    public ResponseEntity<CardServiceResponse>deleteCard(@PathVariable String pan){
        System.out.println("deleteCard");
        System.out.println("El PAN es: "+pan);
        CardServiceResponse result = this.cardService.delete(pan);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PostMapping("")
    public ResponseEntity<CardServiceResponse>addCard(@RequestBody CardModel newCard){
        System.out.println("addCard");
        newCard.toString();
        CardServiceResponse result = this.cardService.add(newCard);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PatchMapping("")
    public ResponseEntity<CardServiceResponse> patchCard (@RequestBody CardModel card){
        System.out.println(card.toString());
        System.out.println("patchCard");
        CardServiceResponse result = this.cardService.partialUpdate(card);
        return new ResponseEntity<>(result, result.getHttpStatus());

    }
}
