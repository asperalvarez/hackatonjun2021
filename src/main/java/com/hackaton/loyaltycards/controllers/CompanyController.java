package com.hackaton.loyaltycards.controllers;

import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.services.CompanyService;
import com.hackaton.loyaltycards.services.CompanyServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/loyalty/v1/company")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping("")
    public ResponseEntity<List<CompanyModel>>getCompanies(){
        System.out.println("getCompanies");
        return new ResponseEntity<>(this.companyService.findAll(), HttpStatus.OK);
    }
    @GetMapping("{id}")
    public ResponseEntity<CompanyServiceResponse>getCompanyById(@PathVariable String id){
        System.out.println("getCompanyById");
        System.out.println("La id es: "+id);
        CompanyServiceResponse result = this.companyService.findById(id);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @DeleteMapping("{id}")
    public ResponseEntity<CompanyServiceResponse>delete(@PathVariable String id){
        System.out.println("deleteCompany");
        System.out.println("La id es: "+id);
        CompanyServiceResponse result = this.companyService.delete(id);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PostMapping("")
    public ResponseEntity<CompanyServiceResponse>addCompany(@RequestBody CompanyModel newCompany){
        System.out.println("addCompany");
        System.out.println("El CIF de la compañía: "+newCompany.getCif());
        System.out.println("El nombre de la compañía: "+newCompany.getNombre());
        System.out.println("La direccion de la compañía: "+newCompany.getDireccion());
        CompanyServiceResponse result = this.companyService.add(newCompany);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PatchMapping("")
    public ResponseEntity<CompanyServiceResponse> patchProduct (@RequestBody CompanyModel company){
        System.out.println("patchCompany");
        System.out.println("El cif de la empresa es: " + company.getCif());
        System.out.println("El nuevo nombre de la empresa actualizado es:  " + company.getNombre());
        System.out.println("La nueva dirección de la empresa es:  " + company.getDireccion());
        CompanyServiceResponse result = this.companyService.partialUpdate(company);
        return new ResponseEntity<>(result, result.getHttpStatus());

    }
}