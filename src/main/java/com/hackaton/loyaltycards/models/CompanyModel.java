package com.hackaton.loyaltycards.models;


import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Companies")
public class CompanyModel {

    @Id
    @NonNull
    private String cif;
    @NonNull
    private String nombre;
    private String direccion;

    public CompanyModel() {
    }

    public CompanyModel(String cif, String nombre) {
        this.cif = cif;
        this.nombre = nombre;
    }

     public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
