package com.hackaton.loyaltycards.models;

import java.util.Date;

public class MovementModel {

    private String fecha;
    private String concepto;
    private int cantidad;

    public MovementModel(String fecha, String concepto, int cantidad) {
        this.fecha = fecha;
        this.concepto = concepto;
        this.cantidad = cantidad;
    }

    public MovementModel() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\nfecha: ");
        sb.append(fecha);
        sb.append("\nconcepto: ");
        sb.append(concepto);
        sb.append("\ncantidad: ");
        sb.append(cantidad);
        return sb.toString();
    }
}
