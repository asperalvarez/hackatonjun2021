package com.hackaton.loyaltycards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoyaltycardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoyaltycardsApplication.class, args);
	}

}
