package com.hackaton.loyaltycards.services;

import com.hackaton.loyaltycards.models.CardModel;
import com.hackaton.loyaltycards.models.MovementModel;
import com.hackaton.loyaltycards.repositories.CardRepository;
import com.hackaton.loyaltycards.repositories.CompanyRepository;
import com.hackaton.loyaltycards.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    CardRepository cardRepository;

    public List<CardModel> findAll (){
        return this.cardRepository.findAll();
    }
    public CardServiceResponse findById (String pan){
        CardServiceResponse result = new CardServiceResponse();
        if(this.cardRepository.findById(pan).isPresent()){
            result.setMsg("Tarjeta encontrada");
            result.setCard(this.cardRepository.findById(pan).get());
            result.setHttpStatus(HttpStatus.FOUND);
            return result;
        }
        result.setMsg("Tarjeta no existente");
        result.setCard(new CardModel(pan, "", ""));
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
    public CardServiceResponse add(CardModel card){
        CardServiceResponse result = new CardServiceResponse();
        if ((this.cardRepository.findById(card.getPan()).isEmpty()) &&
                (this.userRepository.findById(card.getDni()).isPresent()) &&
                (this.companyRepository.findById(card.getCif()).isPresent())){
            this.cardRepository.save(card);
            result.setCard(card);
            result.setMsg("Tarjeta creada correctamente");
            result.setHttpStatus(HttpStatus.OK);
            return result;
        }
        result.setCard(card);
        if (this.cardRepository.findById(card.getPan()).isPresent()){
            result.setMsg("Tarjeta ya existente");
        }else if (this.userRepository.findById(card.getDni()).isEmpty()){
            result.setMsg("Usuario no existente");
        }else{
            result.setMsg("Empresa no existente");
        }
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
    public CardServiceResponse delete (String pan){
        CardServiceResponse result = new CardServiceResponse();
        if(this.cardRepository.findById(pan).isPresent()){
            result.setMsg("Tarjeta borrada");
            result.setCard(this.cardRepository.findById(pan).get());
            result.setHttpStatus(HttpStatus.OK);
            this.cardRepository.delete(this.cardRepository.findById(pan).get());
            return result;
        }
        result.setMsg("Tarjeta no existente");
        result.setCard(new CardModel(pan, "", ""));
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
    public CardServiceResponse partialUpdate (CardModel card){
        CardServiceResponse result = new CardServiceResponse();
        if (this.cardRepository.findById(card.getPan()).isPresent()){
            CardModel aux = this.cardRepository.findById(card.getPan()).get();
            if (!aux.getCif().equals(card.getCif())) {
                result.setMsg("La empresa no se puede actualizar");
            }else if (!aux.getDni().equals(card.getDni())){
                result.setMsg("El usuario no se puede actualizar");
                result.setCard(card);
                result.setHttpStatus(HttpStatus.FORBIDDEN);
                return result;
            }
            if (aux.getMovs()==null){
                aux.setMovs(card.getMovs());
                for (MovementModel m : aux.getMovs()) {
                    aux.setCantidad(aux.getCantidad() + m.getCantidad());
                }
            }else {
                for (MovementModel m : card.getMovs()) {
                    aux.getMovs().add(m);
                    aux.setCantidad(aux.getCantidad() + m.getCantidad());
                }
            }
            this.cardRepository.save(aux);
            result.setMsg("La tarjeta se ha actualizado correctamente");
            result.setCard(aux);
            result.setHttpStatus(HttpStatus.OK);
            return result;
        }
        result.setMsg("Tarjeta no existente");
        result.setCard(card);
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
}
