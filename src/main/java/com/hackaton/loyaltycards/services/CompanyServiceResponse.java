package com.hackaton.loyaltycards.services;

import com.hackaton.loyaltycards.models.CompanyModel;
import org.springframework.http.HttpStatus;

public class CompanyServiceResponse {
    private String msg;
    private CompanyModel company;
    private HttpStatus httpStatus;

    public CompanyServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CompanyModel getCompany() {
        return company;
    }

    public void setCompany(CompanyModel company) {
        this.company = company;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
