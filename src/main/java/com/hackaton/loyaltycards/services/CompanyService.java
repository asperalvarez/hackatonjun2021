package com.hackaton.loyaltycards.services;

import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public List<CompanyModel> findAll(){
        return this.companyRepository.findAll();
    }

    public CompanyServiceResponse findById(String id){
        CompanyServiceResponse res = new CompanyServiceResponse();
        if(this.companyRepository.findById(id).isPresent()) {
            res.setMsg("Compañia encontrada");
            res.setCompany(this.companyRepository.findById(id).get());
            res.setHttpStatus(HttpStatus.OK);
            return res;
        }
        res.setMsg("La compañia no se ha encontrado");
        res.setHttpStatus(HttpStatus.NOT_FOUND);
        return res;
    }
    public CompanyServiceResponse add(CompanyModel company){
        System.out.println("add");
        CompanyServiceResponse res = new CompanyServiceResponse();
        if (this.companyRepository.findById(company.getCif()).isPresent()){
            res.setMsg("Ya existe una compañía con ese Id");
            res.setCompany(company);
            res.setHttpStatus(HttpStatus.BAD_REQUEST);
            return res;

        }else if(company.getCif().isEmpty() || company.getNombre().isEmpty()){
            res.setMsg("Es necesario informar los campos nombre y CIF");
            res.setCompany(company);
            res.setHttpStatus(HttpStatus.BAD_REQUEST);
            return res;

        }
        this.companyRepository.save(company);
        res.setMsg("Compañia guardada correctamente");
        res.setCompany(company);
        res.setHttpStatus(HttpStatus.OK);
        return res;


    }
    public CompanyServiceResponse delete (String id){
        System.out.println("removeById");
        CompanyServiceResponse res = new CompanyServiceResponse();
        if (this.companyRepository.findById(id).isPresent()){
            res.setMsg("Compañia borrada correctamente");
            res.setCompany(this.companyRepository.findById(id).get());
            this.companyRepository.delete(this.companyRepository.findById(id).get());
            res.setHttpStatus(HttpStatus.OK);
            return res;
        }
        res.setMsg("Compañia no encontrada");
        res.setCompany(null);
        res.setHttpStatus(HttpStatus.BAD_REQUEST);
        return res;
    }
    public CompanyServiceResponse partialUpdate (CompanyModel company){
        System.out.println("partialUpdate");
        CompanyServiceResponse res = new CompanyServiceResponse();
        if (this.companyRepository.findById(company.getCif()).isPresent()) {
            CompanyModel result = this.companyRepository.findById(company.getCif()).get();
            if (result.getCif() != null) {
                if (company.getCif().equals(result.getCif()) && company.getCif().equals(result.getCif())) {
                    if (company.getNombre() != null) {
                        System.out.println("Actualizando Nombre");
                        result.setNombre(company.getNombre());
                    }
                    if (company.getDireccion() != null) {
                        System.out.println("Actualizando dirección");
                        result.setDireccion(company.getDireccion());
                    }
                    this.companyRepository.save(result);
                    res.setMsg("Compañía actualizada correctamente");
                    res.setCompany(result);
                    res.setHttpStatus(HttpStatus.OK);
                    return res;

                }
                res.setMsg("El CIF no se pueden mofidicar. Hay que crear una nueva compañia");
                res.setHttpStatus(HttpStatus.BAD_REQUEST);
                return res;
            }
        }
        res.setMsg("La compañia no se ha encontrado");
        res.setHttpStatus(HttpStatus.NOT_FOUND);
        return res;
    }
}
