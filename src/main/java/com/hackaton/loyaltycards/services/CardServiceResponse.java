package com.hackaton.loyaltycards.services;



import com.hackaton.loyaltycards.models.CardModel;
import org.springframework.http.HttpStatus;

public class CardServiceResponse {
    private String msg;
    private CardModel card;
    private HttpStatus httpStatus;

    public CardServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CardModel getCard() {
        return card;
    }

    public void setCard(CardModel card) {
        this.card = card;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}


