package com.hackaton.loyaltycards.services;

import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.models.UserModel;
import org.springframework.http.HttpStatus;

public class UserServiceResponse {
    private String msg;
    private UserModel user;
    private HttpStatus httpStatus;

    public UserServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
