package com.hackaton.loyaltycards.repositories;

import com.hackaton.loyaltycards.models.CompanyModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends MongoRepository<CompanyModel,String> {
}
