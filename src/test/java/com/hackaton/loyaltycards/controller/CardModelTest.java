package com.hackaton.loyaltycards.controller;

import com.hackaton.loyaltycards.controllers.CardController;
import com.hackaton.loyaltycards.controllers.CompanyController;
import com.hackaton.loyaltycards.controllers.UserController;
import com.hackaton.loyaltycards.models.CardModel;
import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.models.MovementModel;
import com.hackaton.loyaltycards.models.UserModel;
import com.hackaton.loyaltycards.services.CardServiceResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CardModelTest {

    @Autowired
    CardController testCards;
    @Autowired
    UserController userController;
    @Autowired
    CompanyController companyController;

    CardModel mockCard = new CardModel("12345432123","A2345321","C2343212");

    @Test
    @Order(1)
    public void test1getAllCards(){

        System.out.println("<CompanyControllerTest> test1getAllCards");
        ResponseEntity<List<CardModel>> response = testCards.getAllCards();
        Assert.assertEquals(HttpStatus.OK,response.getStatusCode() );

    }

    @Test
    @Order(2)
    public void test2addCard(){

        System.out.println("<CompanyControllerTest> testAddCompany");

            if(userController.getUserById(this.mockCard.getCif()).getStatusCode()!=HttpStatus.OK){
                    UserModel usuarioPrueba = new UserModel(this.mockCard.getDni(),"Usuario Prueba");
                    userController.addUser(usuarioPrueba);
            }

             if(companyController.getCompanyById(this.mockCard.getCif()).getStatusCode()!=HttpStatus.OK){

                CompanyModel companyPrueba = new CompanyModel(this.mockCard.getCif(),"Compañia Prueba");
                companyController.addCompany(companyPrueba);

            }


             if(testCards.getCardById(this.mockCard.getPan()).getStatusCode()==HttpStatus.FOUND){

                 Assert.assertEquals("Tarjeta encontrada",testCards.getCardById(this.mockCard.getPan()).getBody().getMsg());

                 testCards.deleteCard(this.mockCard.getPan());

             }

            ResponseEntity<CardServiceResponse> response2= testCards.addCard(this.mockCard);

            Assert.assertEquals("Tarjeta creada correctamente",response2.getBody().getMsg());

            Assert.assertEquals(HttpStatus.OK, response2.getStatusCode());



        }


    @Test
    @Order(3)
    public void test3getCardById(){

        if(testCards.getCardById(this.mockCard.getPan()).getStatusCode()==HttpStatus.FOUND){

            Assert.assertEquals("Tarjeta encontrada",testCards.getCardById(this.mockCard.getPan()).getBody().getMsg());
        }else{

            Assert.assertEquals("Tarjeta no existente",testCards.getCardById(this.mockCard.getPan()).getBody().getMsg());
        }

    }

    @Test
    @Order(4)
    public void test4deleteCard(){

        if(testCards.getCardById(this.mockCard.getPan()).getStatusCode()==HttpStatus.BAD_REQUEST) {

            testCards.addCard(this.mockCard);

        }

        ResponseEntity<CardServiceResponse> response = testCards.deleteCard(this.mockCard.getPan());

        Assert.assertEquals("Tarjeta borrada",response.getBody().getMsg());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());



    }

    @Test
    @Order(5)
    public void test5patchCard(){

        if(testCards.getCardById(this.mockCard.getPan()).getStatusCode()==HttpStatus.BAD_REQUEST) {

            testCards.addCard(this.mockCard);

        }

        this.mockCard.setCantidad(50000);

        List<MovementModel> listaMov= new ArrayList<>();
        this.mockCard.setCantidad(50000);

        this.mockCard.setMovs(listaMov);
        ResponseEntity<CardServiceResponse> response= testCards.patchCard(this.mockCard);

        Assert.assertEquals("La tarjeta se ha actualizado correctamente",response.getBody().getMsg());


    }

}
