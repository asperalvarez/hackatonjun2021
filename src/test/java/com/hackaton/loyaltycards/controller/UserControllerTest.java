package com.hackaton.loyaltycards.controller;


import com.hackaton.loyaltycards.controllers.UserController;
import com.hackaton.loyaltycards.models.UserModel;
import com.hackaton.loyaltycards.services.UserServiceResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    UserController testUsers;

    UserModel mockUser = new UserModel("DNI3","Nombre3");


    @Test
    public void test1GetUsers(){

        System.out.println("<UserControllerTest> testGetUsers");

        ResponseEntity<List<UserModel>> response = testUsers.getUsers("nombre");

        org.junit.Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

    }

    @Test
    public void test2AddUser(){

        System.out.println("<UserControllerTest> testAddUser");

        if(testUsers.getUserById(this.mockUser.getDni()).getStatusCode()==HttpStatus.OK){

            org.junit.Assert.assertEquals("Usuario encontrado",testUsers.getUserById(this.mockUser.getDni()).getBody().getMsg());

            testUsers.delete(this.mockUser.getDni());
        }

        ResponseEntity<UserServiceResponse> response= testUsers.addUser(this.mockUser);

        org.junit.Assert.assertEquals("Usuario creado correctamente",response.getBody().getMsg());

        org.junit.Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);

    }

    @Test
    public void test3GetUserById(){

        System.out.println("<UserControllerTest> testGetUserById");
        System.out.println("<UserControllerTest> Incluimos test user...");

        ResponseEntity<UserServiceResponse> response2 = testUsers.getUserById(this.mockUser.getDni());

        if(response2.getStatusCode()==HttpStatus.NOT_FOUND) {

           testUsers.addUser(this.mockUser);


        }

        ResponseEntity<UserServiceResponse> response3 = testUsers.getUserById(this.mockUser.getDni());

        org.junit.Assert.assertEquals("Usuario encontrado",response3.getBody().getMsg());

        org.junit.Assert.assertEquals(response3.getStatusCode(), HttpStatus.OK);

    }


    @Test
    public void test4DeleteUser(){

        System.out.println("<UserControllerTest> testDeleteUser");
        System.out.println("<UserControllerTest> Eliminamos test user...");

        ResponseEntity<UserServiceResponse> response2 = testUsers.getUserById(this.mockUser.getDni());

        if(response2.getStatusCode()==HttpStatus.NOT_FOUND) {

            testUsers.addUser(this.mockUser);


        }

        ResponseEntity<UserServiceResponse> response = testUsers.delete(this.mockUser.getDni());

        org.junit.Assert.assertEquals("Usuario borrado correctamente", response.getBody().getMsg());

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

    }


}
