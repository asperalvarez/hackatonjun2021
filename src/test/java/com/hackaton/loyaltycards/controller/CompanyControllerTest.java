package com.hackaton.loyaltycards.controller;

import com.hackaton.loyaltycards.controllers.CompanyController;
import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.services.CompanyServiceResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyControllerTest {

    @Autowired
    CompanyController testCompanies;

    CompanyModel mockCompany = new CompanyModel("A2345320","Company Fake");

    @Test
    @Order(1)
    public void testAGetCompanies(){

        System.out.println("<CompanyControllerTest> testGetCompanies");
        ResponseEntity<List<CompanyModel>> response = testCompanies.getCompanies();
        Assert.assertEquals(HttpStatus.OK,response.getStatusCode() );

    }

    @Test
    @Order(2)
    public void testBAddCompany(){

        System.out.println("<CompanyControllerTest> testAddCompany");

        ResponseEntity<CompanyServiceResponse> response= testCompanies.addCompany(this.mockCompany);

        Assert.assertEquals("Compañia guardada correctamente",response.getBody().getMsg());

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

    }


    @Test
    @Order(3)
    public void testCGetCompanyById(){

        System.out.println("<CompanyControllerTest> testGetCompanyById");
        System.out.println("<CompanyControllerTest> Incluimos test company...");

        ResponseEntity<CompanyServiceResponse> response2 = testCompanies.getCompanyById(this.mockCompany.getCif());

        if(response2.getStatusCode()==HttpStatus.NOT_FOUND) {

            ResponseEntity<CompanyServiceResponse> response = testCompanies.addCompany(this.mockCompany);

        }

        ResponseEntity<CompanyServiceResponse> response3 = testCompanies.getCompanyById(this.mockCompany.getCif());

            Assert.assertEquals("Compañia encontrada", response3.getBody().getMsg());

            Assert.assertEquals(HttpStatus.OK, response3.getStatusCode());



    }

    @Test
    @Order(4)
    public void testDeleteCompany(){

        System.out.println("<CompanyControllerTest> testDeleteCompany");
        System.out.println("<CompanyControllerTest> Eliminamos test company...");

        ResponseEntity<CompanyServiceResponse> response = testCompanies.getCompanyById(this.mockCompany.getCif());

        if(response.getStatusCode()==HttpStatus.OK) {

            ResponseEntity<CompanyServiceResponse> response2 = testCompanies.delete(this.mockCompany.getCif());

            Assert.assertEquals(response2.getBody().getMsg(),"Compañia borrada correctamente");

            Assert.assertEquals(response2.getStatusCode(), HttpStatus.OK);

        }else{

            Assert.assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
            Assert.assertEquals(response.getBody().getMsg(), "La compañia no se ha encontrado");

        }

    }



}
